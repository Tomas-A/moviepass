<?php
    namespace DAO;

    use Models\Cine as Cine;

    interface ICineDAO
    {
        function Add(Cine $cine);
        function GetAll();
        //function Remove($nombre);
        //function Update($nombre, $direccion, $capacidad, $valorEntrada);

    }
?>