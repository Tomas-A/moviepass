<?php
namespace DAO;

use Models\Pelicula as Pelicula;
use Models\Genero as Genero;

class PeliculaDAO {
    
    private $peliculasList = array();
    private $generosList = array();
    
    public function getPeliculas(){
        $this->retrieveGeneros();
        $this->retrievePeliculas();
        $this->remplazarIdGenero();
        return $this->peliculasList;
    }
    
    private function retrievePeliculas(){

        /* !!!!!!!!EN WAMP FUNCIONA ASI!!!!!!!!!!*/
        
        $json = file_get_contents("https://api.themoviedb.org/3/movie/now_playing?page=1&language=es&api_key=499b6c2316b484f72da9054c9957ca97");
        
        $arrayToDecode = ($json) ? json_decode($json, true) : array();
        $arrayPeliculas = array_shift($arrayToDecode);

        foreach($arrayPeliculas as $valuesArray)
        {
            $peli = new Pelicula($valuesArray);

            array_push($this->peliculasList, $peli);
        }
        
    }
    public function retrieveGeneros(){
        /* !!!!!!!!EN WAMP FUNCIONA ASI!!!!!!!!!!*/                
        $json = file_get_contents("https://api.themoviedb.org/3/genre/movie/list?api_key=499b6c2316b484f72da9054c9957ca97");
        $arrayToDecode = ($json) ? json_decode($json, true) : array();
        $arrayGeneros = array_shift($arrayToDecode);

        foreach($arrayGeneros as $valuesArray)
            {
                $genero = new Genero($valuesArray["id"],$valuesArray["name"]);
                array_push($this->generosList, $genero);
            }
    }

    public function getGeneroPorId($id_buscado){
        $generoARetornar;
        for($i=0; $i < count($this->generosList) ; $i++)
        {
            $id = $this->generosList[$i]->getId();
            if($id == $id_buscado)
            {
                $generoARetornar = $this->generosList[$i];
            }
        }
        return $generoARetornar;
    }

    private function remplazarIdGenero(){
        foreach($this->peliculasList as $pelicula){
            $generos = array_values(array_filter($this->generosList, function ($genero) use ($pelicula) {
                return array_reduce($pelicula->getGeneros(), function ($equal, $id) use ($genero) {
                    return $equal || $id === $genero->getId();
                });
            }, ARRAY_FILTER_USE_BOTH));
            $pelicula->setGeneros($generos);
        }
    }
    
}
?>
