<?php
    namespace DAO;

    use Models\Pelicula as Pelicula;

    interface IPeliculaDAO
    {
        function GetAll();
        function getByGenero($genero);
    }
?>