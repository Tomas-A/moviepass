<?php
    namespace DAO;

    use DAO\Connection as Connection;
    use DAO\QueryType as QueryType;
    use DAO\ICineDAO as ICineDAO;
    use Models\Cine as Cine;

    class CineDAO implements ICineDAO
    {
        private $connection;
        private $tableName = "cinemas";

        public function Add(Cine $cine)
        {
            $query = "CALL Cinema_Add(?, ?, ?, ?)";

            $parameters["name_cinema"] = $cine->getNombre();
            $parameters["adress"] = $cine->getDireccion();
            $parameters["quantity"] = $cine->getCapacidad();
            $parameters["price"] = $cine->getValorEntrada();

            $this->connection = Connection::GetInstance();

            $this->connection->ExecuteNonQuery($query, $parameters, QueryType::StoredProcedure);
        }

        public function GetAll()
        {
            $cinemaList = array();

            $query = "CALL cinemas_GetAll()";

            $this->connection = Connection::GetInstance();

            $result = $this->connection->Execute($query, array(), QueryType::StoredProcedure);

            foreach($result as $row)
            {
                $cinema = new Cine();
                $cinema->setId($row["id"]);
                $cinema->setNombre($row["name_cinema"]);
                $cinema->setDireccion($row["adress"]);
                $cinema->setCapacidad($row["quantity"]);
                $cinema->setvalorEntrada($row["price"]);

                array_push($cinemaList, $cinema);
            }
            return $cinemaList;
        }


    }
?>