<?php
namespace Models;

use Models\Genero as Genero;

class Pelicula{

    private $id;
    private $titulo;    //string
    private $imagen;    //byte
    private $lenguaje;  //string
    private $sinopsis;
    private $generos;


    public function __construct($value){
        $this->id = $value['id'];
        $this->imagen = $value['poster_path'];
        $this->titulo = $value['title'];
        $this->sinopsis = $value['overview'];
        $this->lenguaje = $value["original_language"];
        $this->generos = $value["genre_ids"];
    }




    public function getId(){
        return $this->id;
    }
    public function getTitulo(){
        return $this->titulo;;
    }
    public function getImagen(){
        return $this->imagen;
    }
    public function getLenguaje(){
        return $this->lenguaje;
    }
    public function getDuracion(){
        return $this->duracion;
    }

    public function getSinopsis(){
        return $this->sinopsis;
    }

    public function getGeneros(){
        return $this->generos;
    }

    public function setGeneros($generos){
        $this->generos = $generos;
    }

}


?>