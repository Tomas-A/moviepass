<?php 
namespace Models;

class Genero{

    private $id;
    private $descripcion; //string

    public function __construct($id, $descripcion){
        $this->id = $id;
        $this->descripcion = $descripcion;
    }

    public function getId(){
        return $this->id;
    }
    public function setId($id){
        $this->id = $id;
    }


    public function getDescripcion(){
        return $this->descripcion;
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
}

?>