<?php 
 include('header.php');
 include('nav-bar-admin.php');
 include("validate-session.php");
?>

<div class="col-sm-12 float-right">
                <!-- Columna de peliculas actuales-->
                <h1 class="mb-5">Listado de Peliculas</h1>
            <?php
                foreach($peliculaList as $pelis){ ?>
                    <div class="row justify-content-md-center ">
                        <div class="col-sm-11">
                            <div class="card w-100 mt-3">
                                <div class="card-header">
                                    <strong><?php echo $pelis->getTitulo(); ?></strong>
                                    
                                </div>
                                
                                <div class="card-body">
                                    <div class="col-sm-4 float-left">
                                        <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2<?php echo $pelis->getImagen();?>" class="card-img-top w-50 mx-5" alt="">  
                                    </div>
                                    <div class="col-sm-8 float-right">
                                        <h6 class="card-title mb-2 text-muted">Sinopsis:</h6>
                                            <p class="card-text">
                                                <?php echo $pelis->getSinopsis(); ?>
                                            </p>
                                        <h6 class="card-title mb-2 text-muted">Genero:</h6>
                                            <p class="card-text">
                                                <?php   $arrayGeneros = $pelis->getGeneros();
                                                        $nombresGeneros = array();
                                                        
                                                        foreach($arrayGeneros as $genero){
                                                            
                                                            array_push($nombresGeneros, $genero->getDescripcion());
                                                        
                                                        }
                                                
                                                        $arrayToString = implode(" / ", $nombresGeneros );
                                                        echo $arrayToString;
                                                ?>
                                            </p>
                                    </div>
                                </div>
                                
                                <div class="card-footer text-center">
                                    <a href="#<?php //echo $pelis->getId(); ?>" class="btn btn-outline-success my-2 my-sm-0">Ver detalles</a>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php } ?>
        </div>  
</div>

<?php include('footer.php'); ?>