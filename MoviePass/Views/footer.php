    
    <div class="wrapper row6">
      <div id="copyright" class="clear" align="center"> 
        <p>Copyright &copy; <?php echo date('Y'); ?> - All Rights Reserved - UTN Laboratorio IV</p>
      </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo JS_PATH ;?>jquery.min.js"></script>
    <script src="<?php echo JS_PATH ;?>bootstrap.min.js"></script>
    <script type="text/javascript">
      if(window.init){
        window.init($);
      }
    </script>
  </body>
</html>