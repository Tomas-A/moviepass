<?php 
 include('header.php');
 include('nav-bar-admin.php');
 /*include("validate-session.php");*/
?>

<main class="p-5">
        <div class="container">
            <h1 class="mb-5">Listado de Cines</h1>

                <div class="form-group mb-4">
                    <button type="button" class="btn btn-light mr-4" data-toggle="modal" data-target="#form-cine">
                        <object type="image/svg+xml" data="<?php echo ICONS_PATH."plus.svg"?>" width="16" height="16"></object>
                    </button>
                </div>

                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nombre</th>
                            <th>Direccion</th>
                            <th>Capacidad</th>
                            <th>Valor Entrada</th>

                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>

                    <?php
                        foreach($cinesList as $cine){ 
                            if($cine->getExist()){
                                ?>                           
                            <tr id="row-<?php echo str_replace(' ', '', $cine->getNombre()); ?>">

                                <td><?php echo $cine->getNombre(); ?></td>
                                <td><?php echo $cine->getDireccion(); ?></td>
                                <td><?php echo $cine->getCapacidad(); ?></td>
                                <td><?php echo $cine->getValorEntrada(); ?></td>

                                <td>
                                <div class="form-inline">
                                    <form action="<?php echo FRONT_ROOT."Cine/Remove"?>">
                                        <button type="submit" name="remove" class="btn" value="<?php echo $cine->getNombre() ?>"> 
                                            <object type="image/svg+xml" data="<?php echo ICONS_PATH."trash-2.svg"?>" width="16" height="16">
                                                Your browser does not support SVG
                                            </object>
                                        </button>
                                    </form>

                                    <button class="btn btn--edit" data-container="#row-<?php echo str_replace(' ', '', $cine->getNombre()); ?>" data-toggle="modal" data-target="#form-cine"> 
                                        <object type="image/svg+xml" data="<?php echo ICONS_PATH."edit.svg"?>" width="16" height="16">
                                            Your browser does not support SVG
                                        </object>
                                    </button>
                                </div>
                                </td>
                            </tr>  
                            <?php
                            }
                        }
                        ?>

                    </tbody>
                </table>
        </div>
    </main>

    <!--
        ADD CIne
    -->
    <div class="modal fade" id="form-cine" tabindex="-1" role="dialog" aria-labelledby="sign-up" aria-hidden="true">
        <div class="modal-dialog" role="document">

            <form class="modal-content" action="<?php echo FRONT_ROOT ?>Cine/Middleware" method="POST">

                <div class="modal-header">
                    <h5 class="modal-title">Registrar Cine</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                                   
                    <div class="form-group">
                        <label>Nombre</label>
                        <input type="text" class="form-control" name="nombre" required/>
                    </div>

                
                    <div class="form-group">
                        <label>Direccion</label>
                        <input type="text" class="form-control" name="direccion" required/>
                    </div>
                

        
                    <div class="form-group">
                        <label>Capacidad</label>
                        <input type="text" class="form-control" name="capacidad" required/>
                    </div>
            
                
                
                    <div class="form-group">
                        <label>Valor Entrada</label>
                        <input type="text" class="form-control" name="valorEntrada" required/>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-dark">Agregar</button>
                </div>
            </div>
                <input type="hidden" name="method" value="Add"/>
            </form>

        </div>
    </div>
<script type="text/javascript">
    function init($) {
        $('#form-cine').on('hidden.bs.modal', function (e) {
            $("#form-cine .form-control[name='nombre']").val('')
            $("#form-cine .form-control[name='direccion']").val('')
            $("#form-cine .form-control[name='capacidad']").val('')
            $("#form-cine .form-control[name='valorEntrada']").val('')
            $("#form-cine .form-control[name='nombre']").attr('readonly', false)
            $('#form-cine .modal-title').text("Registrar Cine");
            $('#form-cine .modal-footer > button[type=submit]').text("Agregar")
            $('#form-cine input[name=method]').val('Add')
        })
        $(".btn--edit").click(function() {
            $('#form-cine .modal-title').text("Modificar Cine");
            $('#form-cine .modal-footer > button[type=submit]').text("Modificar")
            $('#form-cine input[name=method]').val('Update')
            var cine = {};
            var containerQuery = $(this).data("container");
            var container = $(containerQuery+ '> td')
            for (var i = 0; i < container.length; i++) {
                switch (i) {
                    case 0:
                        cine.nombre = $(container[i]).text();
                    break;
                    case 1:
                        cine.direccion = $(container[i]).text();
                    break;
                    case 2:
                        cine.capacidad = $(container[i]).text();
                    break;
                    case 3:
                        cine.valorEntrada = $(container[i]).text();
                    break;
                
                    default:
                        break;
                }
            }

            for (var attr in cine) {
                var element = $("#form-cine .form-control[name='" + attr + "']");
                element.val(cine[attr]);
                if(attr === 'nombre'){
                    element.attr('readonly', true)
                }
            }
        })
    }
</script>

<?php include('footer.php'); ?>
