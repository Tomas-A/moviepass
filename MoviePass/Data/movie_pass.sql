CREATE DATABASE IF NOT EXISTS movie_pass;

USE movie_pass;

CREATE TABLE IF NOT EXISTS cinemas
(
    id INT NOT NULL AUTO_INCREMENT,
    name_cinema VARCHAR(40) NOT NULL UNIQUE,
    adress VARCHAR (40) NOT NULL,
    quantity INT NOT NULL,
    price DECIMAL(10,2) NOT NULL,
    CONSTRAINT pk_cinema PRIMARY KEY (id)
);

DROP procedure IF EXISTS `Cinema_Add`;

DELIMITER $$

CREATE PROCEDURE Cinema_Add (IN name_cinema VARCHAR(40), IN adress VARCHAR (40), IN quantity INT, IN price DECIMAL)
BEGIN
    INSERT INTO cinemas
        (cinema.name_cinema, cinema.adress, cinema.quantity, cinema.price)
    VALUES
        (name_cinema, adress, quantity, price);
END$$

DELIMITER;

DROP procedure IF EXISTS `Cinema_GetAll`;

DELIMITER $$

CREATE PROCEDURE Cinema_GetAll ()
BEGIN
    SELECT name_cinema, adress, quantity, price
    FROM cinemas;
END$$

DELIMITER;

DROP procedure IF EXISTS `Cinema_Delete`;

DELIMITE $$

CREATE PROCEDURE Cinema_Delete (IN id INT)
BEGIN
    DELETE
    FROM cinema
    WHERE (cinema.id = id);
END$$

DELIMITER;
