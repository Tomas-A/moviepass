<?php
    namespace Controllers;

    use DAO\PeliculaDAO as PeliculaDAO;
    use Models\Pelicula as Pelicula;

    class PeliculaController
    {
        private $peliculaDAO;

        public function __construct()
        {
            $this->peliculaDAO = new PeliculaDAO();
        }

        public function ShowListView()
        {
            $peliculaList = $this->peliculaDAO->getPeliculas();

            require_once(VIEWS_PATH."peliculas-list.php");
        }

    }