<?php
    namespace Controllers;

    use DAO\CineDAO as CineDAO;
    use Models\Cine as Cine;

    class CineController
    {
        private $cineDAO;

        public function __construct()
        {
            $this->cineDAO = new CineDAO();
        }

        public function ShowListView()
        {
            require_once(VIEWS_PATH."validate-session.php");
            $cinesList = $this->cineDAO->GetAll();
            require_once(VIEWS_PATH."cines-admin.php");
        }

        public function Add($nombre, $direccion, $capacidad, $valorEntrada)
        {
            require_once(VIEWS_PATH."validate-session.php");

            $cine = new Cine();
            $cine->setNombre($nombre);
            $cine->setDireccion($direccion);
            $cine->setCapacidad($capacidad);
            $cine->setValorEntrada($valorEntrada);
            $cine->setExist(true);

            $this->cineDAO->Add($cine);

            $this->ShowListView();
        }

        public function Middleware($nombre, $direccion, $capacidad, $valorEntrada, $method)
        {
            require_once(VIEWS_PATH."validate-session.php");
            if($method === 'Add'){
                $this->Add($nombre, $direccion, $capacidad, $valorEntrada);
            }
            
            if($method === 'Update'){
                $this->Update($nombre, $direccion, $capacidad, $valorEntrada);
            }
        }

        
        /*public function Remove($nombre)
        {
            require_once(VIEWS_PATH."validate-session.php");
            
            $this->cineDAO->Remove($nombre);

            $this->ShowListView();
        }

        public function Update($nombre, $direccion, $capacidad, $valorEntrada)
        {
            require_once(VIEWS_PATH."validate-session.php");            

            $this->cineDAO->Update($nombre, $direccion, $capacidad, $valorEntrada);

            $this->ShowListView();
        }
        */
        
    }
?>