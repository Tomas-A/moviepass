<?php
    namespace Controllers;

    use DAO\UserDAO as UserDAO;
    use Models\User as User;
    use DAO\CineDAO as CineDAO;

    class HomeController
    {
        private $userDAO;
        private $cineDAO;

        

        public function __construct()
        {
            $this->userDAO = new UserDAO();
            $this->cineDAO = new CineDAO();
        }
 
        public function Index($message = "")
        {
            $cinesList = $this->cineDAO->GetAll();
            require_once(VIEWS_PATH."cines-admin.php");
        }      


        public function ShowAddViewAdmin(){
            require_once(VIEWS_PATH."validate-session.php");
            $cinesList = $this->cineDAO->GetAll();
            require_once(VIEWS_PATH."cines-admin.php");
        }

        public function ShowListViewClient(){

            require_once(VIEWS_PATH."validate-session.php");
            require_once(VIEWS_PATH."peliculas-list.php");
        }

        public function Login($email, $password)
        {
            $user = $this->userDAO->GetByEmail($email);

            if(($user != null) && ($user->getPassword() === $password))
            {
                if($user->getRol() == "admin"){

                    $_SESSION["loggedUser"] = $user;
                    $this->ShowAddViewAdmin();
                }
                else if($user->getRol() == "cliente"){
                    $_SESSION["loggedUser"] = $user;
                    $this->ShowListViewClient();
                }
            }
            else
                $this->Index("Usuario y/o Contraseña incorrectos");
        }
        
        /*
        public function Logout()
        {
            session_destroy();

            $this->Index();
        }
        */
    
    }

?>